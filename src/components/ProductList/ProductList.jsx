import "./ProductList.scss";
import Product from "../Product/Product";

function ProductList({
  products,
  showAddToCartModal,
  checkItemData,
  addToFav,
  favData,
  deleteFavorite,
}) {
  const checkFav = (data) => {
    return favData.some((fav) => {
      return fav.id === data.id;
    });
  };
  return (
    <div className="item-list">
      {products ? (
        products.map((product) => {
          return (
            <Product
              key={product.id}
              checkItemData={checkItemData}
              showAddToCartModal={showAddToCartModal}
              product={product}
              addToFav={addToFav}
              deleteFavorite={deleteFavorite}
              checkFav={checkFav(product)}
            />
          );
        })
      ) : (
        <h1>Product Count 0</h1>
      )}
    </div>
  );
}
export default ProductList;
