import "./Product.scss";
import { BsStarFill } from "react-icons/bs";

function Product({
  product,
  showAddToCartModal,
  checkItemData,
  addToFav,
  checkFav,
  deleteFavorite,
}) {
  const { image, title, price, id } = product;

  return (
    <div className="item">
      <button
        className={`star ${checkFav ? "active" : ""}`}
        onClick={() => {
          checkFav ? deleteFavorite(product) : addToFav(product);
        }}
      >
        <BsStarFill />
      </button>
      <div className="item__content">
        <div className="item__content--img-block">
          <img src={image} />
        </div>
        <div className="item__content--info-block">
          <h3 className="item-title">{title}</h3>
          <p className="item-price">{price} грн</p>
          <p>Артикул: {id}</p>
        </div>
      </div>
      <button
        className="cartBtn"
        onClick={() => {
          showAddToCartModal();
          checkItemData(product);
        }}
      >
        Add to cart
      </button>
    </div>
  );
}
export default Product;
